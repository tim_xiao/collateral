Collateral arrangements are regulated by the Credit Support Annex (CSA) and are always counterparty-based as different counterparties may have different CSA agreements. Thus, financial institutions normally group derivatives into counterparty portfolios first and then process them separately. The difference between counterparties is determined by counterparty credit qualities whereas the difference in collateralization is distinguished by the terms and conditions of CSA agreements.

Collateralization is a critical component of the plumbing of the financial system. The use of collateral in financial markets has increased sharply over the past decade, yet analytical and empirical research on collateralization is relatively sparse. The effect of collateralization on valuation and risk is an understudied area.

Due to the complexity of collateralization, the literature seems to turn away from direct and detailed modeling. For example, Johannes and Sundaresan [2007], and Fuijii and Takahahsi [2012] model collateralization via a cost-of-collateral instantaneous rate. Piterbarg [2010] regards collateral as a regular asset and uses the replication approach to price collateralized derivatives.

Contrary to previous studies, we present a model that characterizes a collateral process directly based on the fundamental principal and legal structure of CSA. The model is devised that allows for collateralization adhering to bankruptcy laws. As such, it can back out price changes due to counterparty risk and collateral posting. Our model is very useful for valuing off-the-run or outstanding derivatives.

This article makes theoretical and empirical contributions to the study of collateralization by addressing several essential questions. First, how does collateralization affect swap rate? 

Interest rate swaps collectively account for two-thirds of all outstanding derivatives. An ISDA mid-market swap rate is based on a mid-day polling. Dealers use this market rate as a reference and make some adjustments to quote an actual swap rate. The adjustment or swap premium is determined by many factors, such as credit risk, liquidity risk, funding cost, operational cost and expected profit, etc. 

Unlike generic mid-market swap rates, swap premia are determined in a competitive market according to the basic principles of supply and demand. A swap client first contacts a number of swap dealers for a quotation and then chooses the most competitive one. If a premium is too low, the dealer may lose money. If a premium is too high, the dealer may lose the competitive advantage.

Unfortunately, we do not know the detailed allocation of a swap premium, i.e., what percentage of the adjustment is charged for each factor. Thus, a direct empirical assessment of the impact of collateralization on swap rate is impossible. 

To circumvent this difficulty, this article uses an indirect empirical approach. We define a swap premium spread as the premium difference between two swap contracts that have exactly the same terms and conditions but are traded with different CSA counterparties. We reasonably believe that if two contracts are identical except counterparties, the swap premium spread should reflect counterparty credit risk only, as all other risks/costs are identical. 

Empirically, we obtain a unique proprietary dataset from an investment bank. We use these data and a statistical measurement   to examine whether credit risk and collateralization, alone or in combination, are sufficient to explain market swap premium spreads. We first study the marginal impact of credit risk. Since credit default swap (CDS) premium theoretically reflects the credit risk of a firm, we use the market swap premium spreads as the response variable and the CDS premium differences between two counterparties as the explanatory variable. The estimation result shows that the adjusted   is 0.7472, implying that approximately 75% of market spreads can be explained by counterparty credit risk. In other words, counterparty risk alone can provide a good but not overwhelming prediction on spreads.

We then assess the joint effect. Because implied or model-generated spreads take into account both counterparty risk and collateralization, we assign the model-implied spreads as the explanatory variable and the market spreads as the response variable. The new adjusted   is 0.9906, suggesting that counterparty risk and collateralization together have high explanatory power on premium spreads. The finding leads to practical implications, such as collateralization modeling allows forecasting credit spread.

Second, how does collateralization affect counterparty credit risk? Credit value adjustment (CVA) is the most prominent measurement in counterparty credit risk. We select all the CSA counterparty portfolios in the dataset and then compute their CVAs. We find that the CVA of a collateralized counterparty portfolio is always smaller than the one of the same portfolio without collateralization. We also find that credit risk is negatively correlated with collateralization as an increase in collateralization causes a decrease in credit risk. The empirical tests corroborate our theoretical conclusions that collateralization can reduce CVA charges and mitigate counterparty risk.

Finally, how do collateralization and credit risk, either alone or in combination, impact market risk? How do they interact with each other? Value at risk (VaR) is the regulatory measurement for market risk We compute VaR in three different cases – VaR without taking credit risk into account, VaR with credit risk, and VaR with both credit risk and collateralization. We find that there is a positive correlation between market risk and credit risk as VaR increases after considering counterparty credit risk. We also find that collateralization and market risk have a negative correlation, i.e., collateral posting can actually reduce VaR. This finding contradicts the prevailing belief in the market that collateralization would increase market risk (see Collateral Management – Wikipedia).

This article addresses an important topic of the impact of collateralization on valuation and risk. We present a new model for pricing collateralized financial contracts based on the fundamental principal and legal structure of CSA. The model can back out market prices. This is very useful for pricing outstanding collateralized derivatives.

Empirically, we use a unique proprietary dataset to measure the effect of collateralization on pricing and compare it with model-implied prices. The empirical results show that the model-implied prices are quite close to the market-quoted prices, suggesting that the model is fairly accurate on pricing collateralized derivatives.

We find strong evidence that counterparty credit risk alone plays a significant but not overwhelming role in determining credit-related spreads. Only the joint effect of collateralization and credit risk has high explanatory power on unsecured credit costs. This finding suggests that failure to properly account for collateralization may result in significant mispricing of derivatives.

We also find evidence that there is a strong linkage between market and credit risk. Our research results suggest that banks and regulators need to think about an integrated framework to capture material interactions of these two types of risk. This requires all profits and losses are gauged in a consistent way across risk types as they tend to be driven by the same economic factors. Our finding leads to an improved understanding of the interaction between market and credit risk and how this interaction is related to risk measurement and management.

References

BCBS. “Findings on the interaction of market and credit risk.” Working paper No. 16, (2009).

Collin-Dufresne, P. and Solnik, B. “On the term structure of default premia in the swap and LIBOR markets.”  Journal of Finance 56 (2001), pp.1095-1115.

Duffie, D. and Huang, M. “Swap rates and credit quality.” Journal of Finance, 51(1996), pp. 921-949.

Duffie, D., and K.J. Singleton, K.J. “Modeling term structure of defaultable bonds.” Review of Financial Studies, 12(1999), pp. 687-720.

Edwards, F. and Morrison, E. “Derivatives and the bankruptcy code: why the special treatment?” Yale Journal on Regulation, 22 (2005), pp. 101-133.

Feldhutter, P. and Lando, D. “Decomposing swap spreads. Journal of Financial Economics,” 88 (2008), pp. 375-405.

FinPricing, 2017, Market Data, https://finpricing.com/lib/IrCurveIntroduction.html

Fuijii, M. and Takahahsi, A. “Collateralized credit default swaps and default dependence: Implications for the central counterparties.”  Journal of Credit Risk, 8(2012), pp. 1-10.

Garlson, D. “Secured creditors and the eely character of bankruptcy valuations.” 41 American University Law Review, 63 (1991-1992), pp. 63-106.

Grinblatt, M., “An analytic solution for interest rate swap spreads.” Review of International Finance, 2 (2001), pp. 113-149.

He, H. “Modeling Term Structures of Swap Spreads.” Working paper, Yale School of Management, Yale University, (2001).

ISDA. “ISDA margin survey 2013.” 2013

Johannes, M. and Sundaresan, S. “The impact of collateralization on swap rates.” Journal of Finance, 62 (2007), pp. 383-410.

J.P. Morgan, “Par credit default swap spread approximation from default probabilities.” 2001.

Liu, J., Longstaff, F. and Mandell, R. “The Market Price of Risk in Interest Rate Swaps: The Roles of Default and Liquidity Risks,” The Journal of Business, 79 (2006), pp. 2337-2360.

Minton, B. “An Empirical Examination of Basic Valuation Models for Plain Vanilla U.S. Interest Rate Swaps.” Journal of Financial Economics, 44 (1997), pp. 251-277.

Moody’s Investor’s Service. “Historical default rates of corporate bond issuers, 1920-99.” 2000

Piterbarg, V. “Funding beyond discounting: collateral agreements and derivatives pricing.” Risk Magazine, 2010 (2010), pp. 97-102.

Routh, R. and Douglas, M. “Default interest payable to oversecured creditor subject to reasonableness limitation.” ABF Journal, Vol. 4, No. 3 (2005), pp. 1-10.

Sorensen, E. and Bollier, T. “Pricing swap default risk.” Financial Analysts Journal, 50 (1994), pp. 23-33.

Xiao, T. “An efficient lattice algorithm for the LIBOR market model.” Journal of derivatives, 19 (2011), pp. 25-40.

Xiao, T. “An accurate solution for credit value adjustment (CVA) and wrong way risk.” Journal of Fixed Income, 25 (2015), pp. 84-95.
